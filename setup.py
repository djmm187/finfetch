from setuptools import setup, find_packages

setup(
    name='finfetch',
    version='0.1',
    description='Fetch SEC financial statements',
    author='nick',
    author_email='djmm187@gmail.com',
    zip_safe=False,
    packages=['finfetch']
)

