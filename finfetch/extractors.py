
RESULTS_MAP = {
    'cik': 'extract_cik',
    'name': 'extract_name',
    'state': 'extract_state'
}


def extract_state(cell):
    """Extracts the state of the company's business address. Since search
    results will contain both people and companies, companies will have
    populated state cells while people will not.

    :param bs4.BeautifulSoup row:
        bs4 parsed table cell

    :returns:
        company's state of incorporation

    """

    return cell.text.strip()


def extract_name(cell):
    """Extracts company name from search results page. Company info cells can
    contain more than just name. For now, lets just split up the contents of
    the cell, and grab only the name.

    :param bs4.BeautifulSoup row:
        bs4 parsed table cell

    :returns:
        company or person's name

    """
    contents = cell.contents[0]

    # from testing, i've only encountered '/'[\ a-zA-Z]',
    # lets just split the string and grab the first string
    # TODO: make this more robust when needed.
    name = contents.split('/')[0]

    return name.strip()


def extract_cik(cell):
    """Extracts cik number from a search results page. CIK numbers are given
    to people ('owners') and companies ('issuers').

    :param bs4.BeautifulSoup row:
        bs4 parsed table cell

    :returns:
        cik int

    """

    return int(cell.text.strip())


def regex_group(pattern, text):
    """Takes a base string and regex pattern (w/ named groups) and returns
    the resulting dict.

    :param str pattern:
        base regex pattern used to extract information
    :param str text:
        string containing target information

    :returns:
        resulting named group dict

    """

    result = {}

    compiled = re.compile(pattern)
    source = text.strip()

    matches = compiled.search(source)

    if matches:
        result = matches.groupdict()

    return result
