import feedparser
import pendulum
import requests


BASE_URL = 'https://www.sec.gov/cgi-bin/browse-edgar'
BASE_PARAMS = {
    'action': 'getcompany',
    'CIK': '',
    'type': '',
    'dateb': '',
    'owner': 'include',
    'start': 0,
    'count': 0,
    'output': 'atom'
}


def format_date(date):
    """Attempts to parse date inline with SEC search format. See inputs:
    https://www.sec.gov/cgi-bin/browse-edgar?CIK=0000789019&action=getcompany

    :param date:
        Timestamp in some form.
    :str date: String date format. '12/12/2012', '2017-08-05'
    :date date: Date object
    :datetime date: Datetime object
    :int date: Millisecond timestamp

    :returns:
        string formatted date (without time) in `%Y%m%d` format

    """

    try:
        date = pendulum.parse(date)
    except:
        print('Error parsing date')
        date = pendulum.now()

    return date.format('%Y%m%d')


def get_params(cik, form_type=None, owner='include', start=1,
               page_size=100, max_date=None):
    """Structures and builds SEC filing request params.

    :param cik:
        Central Index Key given to a company
    :str cik: company's ticker. NOTE: This is does not always work
    :int cik: SEC issued key
    :param str form_type:
        filter by the filing type
    :param str owner:
        Ownership Forms 3, 4, and 5. defaults to `include`.
    :param int start:
        offset for result set (i.e. page)
    :param int page_size:
        max number of results returned within each request
    :param max_date:
        timestamp to restrict to filings before a certain date
        see :meth:`filings.format_date`

    :returns:
        formatted request dict.

    """

    fmt_date = format_date(max_date)

    return {
        **BASE_PARAMS,
        **{
            'dateb': fmt_date,
            'type': form_type,
            'owner': owner,
            'start': start,
            'count': page_size,
            'CIK': cik
        }
    }


def extract_company_info(feed):
    """Takes a feed node or feed object, removes unnecessary data, and
    returns the target companies information.

    :param feedparser.FeedParserDict feed:
        feed or parsed feed dict, i.e. `feed_obj` or `feed_obj._attr_`

    :returns:
        dict containing company metadata

    """

    if hasattr(feed, 'feed'):
        feed = feed.feed

    remove = (
        'authors',
        'author_detail',
        'author',
    )

    for item in remove:
        feed.pop(item, None)

    return feed


def extract_entries(feed):
    """Grabs filing result set for a given page.

    :param feedparser.FeedParserDict feed:
        feed or parsed feed dict, i.e. `feed_obj` or `feed_obj._attr_`

    :returns:
        a list of filing objects from SEC response

    """
    entries = []

    if hasattr(feed, 'entries'):
        entries = feed.entries

    return entries


def extract_nav_links(feed):
    """Grabs the navigation links from a result set page.

    :param feedparser.FeedParserDict feed:
        feed or parsed feed dict, i.e. `feed_obj` or `feed_obj._attr_`

    :returns:
       formatted dict containing prev, next, self, alternate links

    """

    links = {}

    if hasattr(feed, 'feed'):
        feed = feed.feed

    return {l.rel: l.href for l in feed.links}


def get_results(url, params={}):
    """Fetches a result set from an atom feed and returns a feedparser object

    :param str url:
        base url used for request
    :param dict params: (optional)
        any filter criteria for request. for specific filter options, see
        :meth:`filings.get_params`

    :returns:
        a feed parserobject containing SEC result set

    """

    results = None
    resp = requests.get(url, params=params)

    if resp.ok:
        results = resp.text

    return feedparser.parse(results)


def extract_entry_info(entry):
    """Parses and normalizes a high level filing object containing a filing's
    content and associated filing metadata.

    :param dict entry:
        a raw filing response object from a feedparser result

    :returns:
        top level filing metadata

    """

    filing = {}
    omit = [
        'tags',
        'updated_parsed',
        'content',
        'links'
    ]

    for key in entry.keys():
        # skip all expanded properties,
        # we just want the base information
        if '_detail' not in key:

            # skip these, unless someone requests them
            if key in omit:
                continue

            # normalized
            filing[key.replace('-', '_')] = entry[key]

    return filing


def extract_entries(entries):
    """Takes a feed parser response and normalizes the filing objects.

    :param feedparser.FeedParserDict feed:
        feed or parsed feed dict, i.e. `feed_obj` or `feed_obj._attr_`

    :returns:
        list containing normalized filing objects

    """

    filings = []

    # make sure a raw feed object was not passed in
    if hasattr(entries, 'entries'):
        entries = entries.entries

    for entry in entries:
        filings.append(extract_entry_info(entry))

    return filings


def get_all(cik, **kwargs):
    """A convenience  method to return all of the a company's publicly
    available filings. NOTE: depending on the company, this may take a
    bit to compile and is left to the mercy of the SEC's servers depending
    on the number of requests issues, i.e. the number of filings a company
    has issued.

    :param cik:
        Central Index Key given to a company
    :str cik: company's ticker. NOTE: This is does not always work
    :int cik: SEC issued key
    :param \**kwargs: (optional)
        a list of filters when fetching all of the companies filings.
        For filter options, see: :meth:`filings.get_params`

    :returns:
        A compiled list of all filing objects for a target company.

    """

    next_url = None
    filings = []

    # initial set
    results = get_results(BASE_URL, get_params(cik, **kwargs))

    links = extract_nav_links(results.feed)
    next_url = links.get('next', None)
    filings.extend(extract_entries(results))

    while next_url is not None:
        results = get_results(next_url)
        filings.extend(extract_entries(results))

        if hasattr(results, 'links'):
            next_url = extract_nav_links(results.feed).get('next')
        else:
            next_url = None

    return filings
