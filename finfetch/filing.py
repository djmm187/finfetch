import requests


def get_filing(filing):
    """Takes a filing object or a url and grabs the content.

    This is focused on data gathering that will be used for analysis. That
    means that this will return the raw, concatenated txt version.

    :param filing:
        endpoint location for a companies filing
    :str filing: filing endpoint url
    :dict filing: filing object containing `filing_href` property for endpoint

    :returns:
        dict contaiing filing contents within `content` property

    """
    doc = None
    url = filing

    if isinstance(url, dict):
        if 'filing_href' not in url:
            return doc
        else:
            url = filing.get('filing_href')

    extension = url[-3:]

    # check to see if its a raw text link
    # if not, convert it and fetch the doc
    if 'txt' not in extension:
        # we have a text link
        url = html_to_txt_url(url)

    # TODO: When needed or requested, lets return a dict with `content`
    #       content property. We will do a little forward thinking, in case
    #       we need return multiple formats or we need to merge responses
    return {
        'content': get_content(url)
    }


def html_to_txt_url(url):
    """Converts filings html URL endpoint (general page) to a filings text
    endpoint (raw, concatenated).

    :param str url:
        filings URL

    :returns:
        txt endpoint for a single filing

    """

    return url.replace('-index.htm', '.txt')


def get_content(url):
    """Fetches filing contents.

    :param str url:
        filings URL

    :returns:
        raw text contents of a single filing

    """

    filing = None
    response = requests.get(url)

    if response.ok:
        filing = response.text

    return filing
