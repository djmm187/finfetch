import requests

from bs4 import BeautifulSoup
from finfetch import extractors

BASE_URL = 'https://www.sec.gov/cgi-bin/browse-edgar'

CLS = {
    'NO_MATCH': 'noCompanyMatch',
    'MATCH': 'companyMatch',
    'COMPANY_INFO': 'companyInfo',
    'COMPANY_NAME': 'companyName',
    'COMPANY_META': 'identInfo',
    'RESULTS_TABLE': 'tableFile2'
}

FILINGS_PAGE_NAME_CIK_RE = r'(?P<name>.*?)\ CIK\#\:\ (?P<cik>\d*)'
FILINGS_PAGE_STATE_RE = r'State location:\ (?P<state>.*?) \|'


def get_params(pattern):
    """Takes a search pattern and maps to dict used for SEC company
    search. The only supported option is `company`

    :param str pattern:
        ticker or company name (complete or partial)

    :returns:
        dict containing company property. ex: `{'company': 'ABC Inc.'}`

    """

    return {
        'company': pattern
    }


def get_results(url, params={}):
    """Request page and parse the page for search results.

    :param str url:
        SEC search endpoint
    :param dict params:
        request params. Currently the only supported option is
        `company`. ex: `{'company':'foo'}`

    :returns:
        dict containing two lists of people and company search results.
        see :meth: `finfetch.search.parse_results`

    """

    results = []

    resp = requests.get(url, params=params)

    if resp.ok:
        results = parse_results(resp.text)

    return results


def by_name(name):
    """Given a company name, lets return the search results.

    :param str ticker:
        complete or partial company name.

    :returns:
        dict containing two lists of people and company search results.
        see :meth: `finfetch.search.parse_results`

    """

    return get_results(BASE_URL, get_params(name))


def by_ticker(ticker):
    """Given a ticker, lets return the search results.

    :param str ticker:
        company symbol

    :returns:
        dict containing two lists of people and company search results.
        see :meth: `finfetch.search.parse_results`

    """

    return get_results(BASE_URL, get_params(ticker))


def has_matches(soup):
    """Searches the page to make sure there is actual content for us to look.
    We will check to see if there is a no match element, If so, we have nothing
    to do. If not, its either a company page or a search results page.

    :param bs4.BeautifulSoup soup:
        bs4 parsed html page

    :returns:
        bool indicating if the page contains some sort of results for us
        to parse

    """

    has_match = False
    no_match_el = soup.find('div', class_=CLS['NO_MATCH'])

    if not bool(no_match_el):
        has_match = True

    return has_match


def is_company_page(soup):
    """If we have a direct hit, i.e. we have a match and are redirected to a
    company's filings page, we will need a different method to parse out the
    relevant data information. Lets make sure are looking at the correct dom
    structure.

    :param bs4.BeautifulSoup soup:
        bs4 parsed html page

    :returns:
        bool indicating if we have skipped results page and are currently
        parsing a page with a list of company filings.

    """

    is_comp_page = True
    match_el = soup.find('div', class_=CLS['MATCH'])

    if not bool(match_el) or not has_matches(soup):
        is_comp_page = False

    return is_comp_page


def from_results(soup):
    """Parses out cik, name, and state from a search results page. We iterate
    through the rows extracting data and categorizing each row into `companies`
    or `people` bucket.

    :param bs4.BeautifulSoup soup:
        bs4 parsed html page

    :returns:
        dict containing a companies and people results

    """

    tbl = soup.find('table', class_=CLS['RESULTS_TABLE'])
    search_hits = {
        'companies': [],
        'people': []
    }

    for row in tbl.find_all('tr'):
        cells = row.find_all('td')

        if not len(cells):
            continue

        row_data = {}
        row_type = 'companies'

        for i, (label, func) in enumerate(extractors.RESULTS_MAP.items()):
            row_data[label] = getattr(extractors, func)(cells[i])

        if not row_data.get('state'):
            row_type = 'people'

        search_hits[row_type].append(row_data)

    return search_hits


def from_filings(soup):
    """Extracts name, cik, and state from a filings search page. This is used
    when there is a direct match and the SEC redirects the user to company's
    filings results page.

    :param bs4.BeautifulSoup soup:
        bs4 parsed html page

    :returns:
        dict containing a company property with the metadata associated with
        the direct match.

    """

    state_res, name_cik_res = {}, {}
    results = []
    search_hits = {
        'companies': [],
        'people': []
    }
    comp_info = soup.find('div', class_=CLS['COMPANY_INFO'])
    name_cik_cont = comp_info.find('span', class_=CLS['COMPANY_NAME'])
    state_cont = find('p', class_=CLS['COMPANY_META'])

    # name and cik
    if name_cik_cont:
        name_cik_res = extractors.regex_group(
            FILINGS_PAGE_NAME_CIK_RE,
            name_cik_cont.text
        )

    # find state from business address
    if state_cont:
        state_res = extractors.regex_group(
            FILINGS_PAGE_STATE_RE,
            state_cont.text
        )

    results = [{**state_res, **name_cik_res}]
    # lets make sure we have a complete entry before returning results
    if state_res and name_cik_res:
        search_hits['companies'] = results
    else:
        search_hits['people'] = results

    return search_hits


def parse_results(page):
    """Take the raw response from an SEC search request, parse using
    BeautifulSoup, and return all people and company information from the
    query.

    :param str page:
        raw response body. see :meth: `finfetch.search.get_results`

    :returns:
        dict containing people and company properties with search result
        objects

    """

    soup = BeautifulSoup(page, 'html.parser')
    results, search_results = {}, {
        'people': [],
        'companies': []
    }

    # determine results page structure, i.e.
    # direct hit or search results page
    if is_company_page(soup):
        results = from_filings(soup)
    else:
        # make sure we have a single or multiple results
        if has_matches(soup):
            results = from_results(soup)

    search_results.update(results)

    return search_results
