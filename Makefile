SHELL := /bin/bash

#COLORS
GREEN  := $(shell tput -Txterm setaf 2)
WHITE  := $(shell tput -Txterm setaf 7)
YELLOW := $(shell tput -Txterm setaf 3)
RESET  := $(shell tput -Txterm sgr0)

HELP_FUN = \
    %help; \
    while(<>) { push @{$$help{$$2 // 'options'}}, [$$1, $$3] if /^([a-zA-Z\-]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/ }; \
    print "usage: make [target]\n\n"; \
    for (sort keys %help) { \
    print "${WHITE}$$_:${RESET}\n"; \
    for (@{$$help{$$_}}) { \
    $$sep = " " x (32 - length $$_->[0]); \
    print "  ${YELLOW}$$_->[0]${RESET}$$sep${GREEN}$$_->[1]${RESET}\n"; \
    }; \
    print "\n"; }

.PHONY: help

help: ## Show help menu
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)

up: ## Start up containers
	docker-compose up -d

down: ## Stops containers
	docker-compose kill

clean: down ## Removes containers
	docker-compose rm

rebuild: clean up ## Rebuild the container

logs: ## View container logs
	docker-compose logs -t -f --tail=50

deps: ## Install requirements on host machine
	pip install -Ur requirements.txt

docker-deps: ## Install requirements within container
	docker-compose exec finfetch make deps

container: ## Attach to finfetch container
	docker-compose exec finfetch /bin/bash


