FROM python:3.6
MAINTAINER djmm187

WORKDIR /code
COPY . /code

RUN python setup.py install
RUN pip install -e .
RUN pip install -Ur requirements.txt

RUN apt-get update && \
    apt-get install -y netcat

