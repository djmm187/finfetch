from finfetch import filing

doc = {
    "accession_nunber": "0001437749-17-013731",
    "filing_date": "2017-08-03",
    "filing_href": "http://www.sec.gov/Archives/edgar/data/1418091/000143774917013731/0001437749-17-013731-index.htm",
    "filing_type": "4",
    "form_name": "Statement of changes in beneficial ownership of securities: n",
    "size": "4 KB",
    "id": "urn:tag:sec.gov,2008:accession-number=0001437749-17-013731",
    "guidislink": True,
    "link": "http://www.sec.gov/Archives/edgar/data/1418091/000143774917013731/0001437749-17-013731-index.htm",
    "summary": "<b>Filed:</b> 2017-08-03 <b>AccNo:</b> 0001437749-17-013731 <b>Size:</b> 4 KB",
    "title": "4  - Statement of changes in beneficial ownership of securities: n",
    "updated": "2017-08-03T21:08:15-04:00"
}


print(filing.get_filing('https://www.sec.gov/Archives/edgar/data/789019/000119309911000021/0001193099-11-000021-index.htm'))
